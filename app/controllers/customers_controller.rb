# frozen_string_literal: true

require_relative '../services/customer_service'

class CustomersController < ApplicationController

  def index
    @customers = CustomerService.new(params: {}).customers_list
  end

  def show
    @customer = CustomerService.new(
        params: {customer_id: params[:id]}
    ).customer
  end
end
