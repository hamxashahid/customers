# frozen_string_literal: true

require_relative '../services/order_service'

class Customer
  include ActiveModel::Model

  attr_accessor :id,
                :company,
                :first_name,
                :last_name,
                :email,
                :phone,
                :form_fields,
                :date_created,
                :date_modified,
                :store_credit,
                :registration_ip_address,
                :customer_group_id,
                :notes,
                :tax_exempt_category,
                :reset_pass_on_login,
                :accepts_marketing


  def full_name
    "#{first_name} #{last_name}"
  end

  def orders
    order_service.orders_list
  end

  def orders_count
    order_service.orders_count
  end

  private

  def order_service
    @order_service ||= OrderService.new(params: {customer_id: id})
  end
end
