# frozen_string_literal: true

require_relative '../services/order_service'

class Order
  include ActiveModel::Model

  attr_accessor :id,
                :customer_id,
                :date_created,
                :date_modified,
                :date_shipped,
                :status,
                :subtotal_inc_tax,
                :shipping_cost_inc_tax,
                :handling_cost_inc_tax,
                :wrapping_cost_inc_tax,
                :total_inc_tax,
                :total_tax,
                :items_total,
                :items_shipped,
                :payment_method,
                :payment_status,
                :refunded_amount,
                :customer_message,
                :products,
                :custom_status

  def product_count
    order_service.order_products_count
  end

  private

  def order_service
    @order_service ||= OrderService.new(
        params: {customer_id: customer_id, order_id: id}
    )
  end
end
