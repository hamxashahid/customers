# frozen_string_literal: true

require_relative '../../lib/utilities/big_commerce_client'
require_relative '../../lib/utilities/customer_parser'
require_relative '../../lib/utilities/order_parser'

class OrderService

  attr_reader :api_client

  def initialize(params:)
    self.api_client = BigCommerceClient.new(params: params)
  end

  def orders_list
    OrderParser.new(data: orders_data).order_list
  end

  def orders_count
    orders_data.size
  end

  def order_products_count
    api_client.order_products_count
  end

  private

  attr_writer :api_client

  def orders_data
    JSON.parse(api_client.orders)
  end
end
