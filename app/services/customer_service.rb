# frozen_string_literal: true

require_relative '../../lib/utilities/big_commerce_client'
require_relative '../../lib/utilities/customer_parser'
require_relative '../../lib/utilities/order_parser'

class CustomerService

  attr_reader :api_client

  def initialize(params:)
    self.api_client = BigCommerceClient.new(params: params)
  end

  def customers_list
    customers_data = JSON.parse(api_client.customers)
    CustomerParser.new(data: customers_data).customer_list
  end

  def customer
    customer_data = JSON.parse(api_client.customer)
    CustomerParser.new(data: customer_data).customer
  end

  private

  attr_writer :api_client
end
