# frozen_string_literal: trues

class OrderParser

  def initialize(data:)
    self.data = data
  end

  def order_list
    data.reduce([]) do |orders, obj|
      orders << order_object(order_hash: obj)
      orders
    end
  end

  private

  attr_accessor :data

  def order_object(order_hash:)
    order = Order.new.tap do |o|
      o.id = order_hash['id']
      o.customer_id = order_hash['customer_id']
      o.date_created = order_hash['date_created']
      o.date_modified = order_hash['date_modified']
      o.date_shipped = order_hash['date_shipped']
      o.status = order_hash['status']
      o.subtotal_inc_tax = order_hash['subtotal_inc_tax']
      o.shipping_cost_inc_tax = order_hash['shipping_cost_inc_tax']
      o.wrapping_cost_inc_tax = order_hash['wrapping_cost_inc_tax']
      o.total_inc_tax = order_hash['total_inc_tax']
      o.total_tax = order_hash['total_tax']
      o.items_total = order_hash['items_total']
      o.items_shipped = order_hash['items_shipped']
      o.payment_method = order_hash['payment_method']
      o.payment_status = order_hash['payment_status']
      o.refunded_amount = order_hash['refunded_amount']
      o.products = order_hash['products']
      o.custom_status = order_hash['custom_status']
    end
  end
end
