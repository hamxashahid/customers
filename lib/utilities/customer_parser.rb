# frozen_string_literal: true

class CustomerParser

  def initialize(data:)
    self.data = data
  end

  def customer_list
    data.reduce([]) do |customers, obj|
      customers << customer_object(customer_hash: obj)
      customers
    end
  end

  def customer
    customer_object(customer_hash: data)
  end

  private

  attr_accessor :data

  def customer_object(customer_hash:)
    customer = Customer.new.tap do |c|
      c.id = customer_hash['id']
      c.company = customer_hash['company']
      c.first_name = customer_hash['first_name']
      c.last_name = customer_hash['last_name']
      c.email = customer_hash['email']
      c.phone = customer_hash['phone']
      c.form_fields = customer_hash['form_fields']
      c.date_created = customer_hash['date_created']
      c.date_modified = customer_hash['date_modified']
      c.store_credit = customer_hash['store_credit']
      c.registration_ip_address = customer_hash['registration_ip_address']
      c.customer_group_id = customer_hash['customer_group_id']
      c.notes = customer_hash['notes']
      c.tax_exempt_category = customer_hash['tax_exempt_category']
      c.reset_pass_on_login = customer_hash['reset_pass_on_login']
      c.accepts_marketing = customer_hash['accepts_marketing']
    end
  end
end
