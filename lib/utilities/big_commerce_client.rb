# frozen_string_literal: true

class BigCommerceClient

  attr_accessor :params

  def initialize(params: nil)
    self.params = params
  end

  def handle_timeouts
    begin
      yield
    rescue Bigcommerce::TimeOut, Bigcommerce::GatewayTimeout
      {}.to_json
    end
  end

  def customers
    handle_timeouts do
      customers_response = Bigcommerce::System.raw_request(:get, customers_path)
      customers_response&.body.present? ? customers_response.body : {}.to_json
    end
  end

  def customer
    handle_timeouts do
      customer_response = Bigcommerce::System.raw_request(:get, customer_detail_path)
      customer_response&.body.present? ? customer_response.body : {}.to_json
    end
  end

  def orders
    handle_timeouts do
      orders_response = Bigcommerce::System.raw_request(:get, orders_path, customer_id: params[:customer_id])
      orders_response&.body.present? ? orders_response.body : {}.to_json
    end
  end


  def order_products_count
    handle_timeouts do
      Bigcommerce::OrderProduct.count(params[:order_id])&.count
    end
  end

  private

  def customers_path
    'customers'
  end

  def customer_detail_path
    "customers/#{params[:customer_id]}"
  end

  def orders_path
    'orders'
  end
end
