# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BigCommerceClient do

  subject { described_class }


  describe '#customers' do

    context 'when the request is successful' do

      it 'returns response' do
        bigcommerce_client = subject.new(params: {})
        expect(Bigcommerce::System).to receive(:raw_request).with(:get, 'customers')
        response_data = bigcommerce_client.customers
        expect(JSON.parse(response_data)).not_to be_nil
      end
    end

    context 'when the request times out' do

      it 'resques timeout and returns empty response' do
        bigcommerce_client = subject.new(params: {})
        expect(Bigcommerce::System).to receive(:raw_request).with(:get, 'customers').and_raise(Bigcommerce::TimeOut.new('timeout'))
        response_data = bigcommerce_client.customers
        expect(JSON.parse(response_data)).to be_empty
      end
    end
  end

  describe '#customer' do

    context 'when the request is successful' do

      it 'returns response' do
        bigcommerce_client = subject.new(params: {customer_id: '10'})
        expect(Bigcommerce::System).to receive(:raw_request).with(:get, 'customers/10')
        response_data = bigcommerce_client.customer
        expect(JSON.parse(response_data)).not_to be_nil
      end
    end

    context 'when the request times out' do

      it 'resques timeout and returns empty response' do
        bigcommerce_client = subject.new(params: {customer_id: '10'})
        expect(Bigcommerce::System).to receive(:raw_request).with(:get, 'customers/10').and_raise(Bigcommerce::TimeOut.new('timeout'))
        response_data = bigcommerce_client.customer
        expect(JSON.parse(response_data)).to be_empty
      end
    end
  end

  describe '#orders' do

    context 'when the request is successful' do

      it 'returns response' do
        bigcommerce_client = subject.new(params: {customer_id: '10'})
        expect(Bigcommerce::System).to receive(:raw_request).with(:get, 'orders', customer_id: '10')
        response_data = bigcommerce_client.orders
        expect(JSON.parse(response_data)).not_to be_nil
      end
    end

    context 'when the request times out' do

      it 'resques timeout and returns empty response' do
        bigcommerce_client = subject.new(params: {customer_id: '10'})
        expect(Bigcommerce::System).to receive(:raw_request).with(:get, 'orders', customer_id: '10').and_raise(Bigcommerce::TimeOut.new('timeout'))
        response_data = bigcommerce_client.orders
        expect(JSON.parse(response_data)).to be_empty
      end
    end
  end

  describe '#order_products_count' do

    context 'when the request is successful' do

      it 'returns response' do
        bigcommerce_client = subject.new(params: {customer_id: '10', order_id: '115'})
        expect(Bigcommerce::OrderProduct).to receive(:count).with('115')
        bigcommerce_client.order_products_count
      end
    end

    context 'when the request times out' do

      it 'resques timeout and returns empty response' do
        bigcommerce_client = subject.new(params: {customer_id: '10', order_id: '115'})
        expect(Bigcommerce::OrderProduct).to receive(:count).with('115').and_raise(Bigcommerce::TimeOut.new('timeout'))
        response_data = bigcommerce_client.order_products_count
        expect(JSON.parse(response_data)).to be_empty
      end
    end
  end
end
