# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CustomerParser do

  subject { described_class }
  
  let(:customers) do
    [
        {
            'id'=>1,
            'company'=>'',
            'first_name'=>'Jennifer',
            'last_name'=>'Fox',
            'email'=>'customer1091RI6L@gmail.com',
            'phone'=>'',
            'form_fields'=>nil,
            'date_created'=>'Wed, 04 Apr 2018 23:08:28 +0000',
            'date_modified'=>'Wed, 04 Apr 2018 23:08:28 +0000',
            'store_credit'=>'0.0000',
            'registration_ip_address'=>'',
            'customer_group_id'=>2,
            'notes'=>'',
            'tax_exempt_category'=>'',
            'reset_pass_on_login'=>false,
            'accepts_marketing'=>false,
            'addresses'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/customers/1/addresses.json',
                'resource'=>'/customers/1/addresses'
            }
        },
        {
            'id'=>2,
            'company'=>'',
            'first_name'=>'Roosevelt',
            'last_name'=>'Hawes',
            'email'=>'customerOGAMOIHP@gmail.com',
            'phone'=>'',
            'form_fields'=>nil,
            'date_created'=>'Wed, 04 Apr 2018 23:08:28 +0000',
            'date_modified'=>'Wed, 04 Apr 2018 23:08:28 +0000',
            'store_credit'=>'0.0000',
            'registration_ip_address'=>'',
            'customer_group_id'=>1,
            'notes'=>'',
            'tax_exempt_category'=>'',
            'reset_pass_on_login'=>false,
            'accepts_marketing'=>false,
            'addresses'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/customers/2/addresses.json',
                'resource'=>'/customers/2/addresses'
            }
        },
    ]
  end

  let(:customer) do
    {
        'id'=>1,
        'company'=>'',
        'first_name'=>'Jennifer',
        'last_name'=>'Fox',
        'email'=>'customer1091RI6L@gmail.com',
        'phone'=>'',
        'form_fields'=>nil,
        'date_created'=>'Wed, 04 Apr 2018 23:08:28 +0000',
        'date_modified'=>'Wed, 04 Apr 2018 23:08:28 +0000',
        'store_credit'=>'0.0000',
        'registration_ip_address'=>'',
        'customer_group_id'=>2,
        'notes'=>'',
        'tax_exempt_category'=>'',
        'reset_pass_on_login'=>false,
        'accepts_marketing'=>false,
        'addresses'=>{
            'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/customers/1/addresses.json',
            'resource'=>'/customers/1/addresses'
        }
    }
  end

  describe '#customer_list' do 

    it 'parses data and returns customer collection' do
      parser = subject.new(data: customers)
      customer_list = parser.customer_list
      expect(customer_list).to be_an_instance_of(Array)
      expect(customer_list.size).to eq 2
      expect(customer_list.first).to be_an_instance_of(Customer)
      expect(customer_list.last).to be_an_instance_of(Customer)
      expect(customer_list.first.id).to eq 1
      expect(customer_list.last.id).to eq 2
    end
  end

  describe '#customer' do

    it 'parses data and returns customer object' do
      parser = subject.new(data: customer)
      customer = parser.customer
      expect(customer).to be_an_instance_of(Customer)
      expect(customer.id).to eq 1
    end
  end
end