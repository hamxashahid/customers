# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderParser do

  subject { described_class }

  let(:orders) do
    [
        {
            'id'=>115,
            'customer_id'=>10,
            'date_created'=>'Wed, 04 Apr 2018 23:50:45 +0000',
            'date_modified'=>'Wed, 04 Apr 2018 23:50:50 +0000',
            'date_shipped'=>'',
            'status_id'=>11,
            'status'=>'Awaiting Fulfillment',
            'subtotal_ex_tax'=>'56.5000',
            'subtotal_inc_tax'=>'56.5000',
            'subtotal_tax'=>'0.0000',
            'base_shipping_cost'=>'9.9500',
            'shipping_cost_ex_tax'=>'9.9500',
            'shipping_cost_inc_tax'=>'9.9500',
            'shipping_cost_tax'=>'0.0000',
            'shipping_cost_tax_class_id'=>2,
            'base_handling_cost'=>'0.0000',
            'handling_cost_ex_tax'=>'0.0000',
            'handling_cost_inc_tax'=>'0.0000',
            'handling_cost_tax'=>'0.0000',
            'handling_cost_tax_class_id'=>2,
            'base_wrapping_cost'=>'0.0000',
            'wrapping_cost_ex_tax'=>'0.0000',
            'wrapping_cost_inc_tax'=>'0.0000',
            'wrapping_cost_tax'=>'0.0000',
            'wrapping_cost_tax_class_id'=>3,
            'total_ex_tax'=>'66.4500',
            'total_inc_tax'=>'66.4500',
            'total_tax'=>'0.0000',
            'items_total'=>2,
            'items_shipped'=>0,
            'payment_method'=>'Test Payment Gateway',
            'payment_provider_id'=>'',
            'payment_status'=>'captured',
            'refunded_amount'=>'0.0000',
            'order_is_digital'=>false,
            'store_credit_amount'=>'0.0000',
            'gift_certificate_amount'=>'0.0000',
            'ip_address'=>'64.183.182.114',
            'geoip_country'=>'United States',
            'geoip_country_iso2'=>'US',
            'currency_id'=>1,
            'currency_code'=>'USD',
            'currency_exchange_rate'=>'1.0000000000',
            'default_currency_id'=>1,
            'default_currency_code'=>'USD',
            'staff_notes'=>'',
            'customer_message'=>'',
            'discount_amount'=>'0.0000',
            'coupon_discount'=>'0.0000',
            'shipping_address_count'=>1,
            'is_deleted'=>false,
            'ebay_order_id'=>'0',
            'cart_id'=>'64c726f6-490e-47ac-86b9-a9eefb6f8c8a',
            'billing_address'=>{
                'first_name'=>'Juanita',
                'last_name'=>'Jones',
                'company'=>'',
                'street_1'=>'180 County Road 155',
                'street_2'=>'',
                'city'=>'Georgetown',
                'state'=>'Texas',
                'zip'=>'78626',
                'country'=>'United States',
                'country_iso2'=>'US',
                'phone'=>'',
                'email'=>'customer7DVT05CT@gmail.com',
                'form_fields'=>[

                ]
            },
            'is_email_opt_in'=>false,
            'credit_card_type'=>nil,
            'order_source'=>'www',
            'channel_id'=>1,
            'external_source'=>nil,
            'products'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/115/products.json',
                'resource'=>'/orders/115/products'
            },
            'shipping_addresses'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/115/shippingaddresses.json',
                'resource'=>'/orders/115/shippingaddresses'
            },
            'coupons'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/115/coupons.json',
                'resource'=>'/orders/115/coupons'
            },
            'external_id'=>nil,
            'external_merchant_id'=>nil,
            'tax_provider_id'=>'',
            'custom_status'=>'Awaiting Fulfillment'
        },
        {
            'id'=>116,
            'customer_id'=>10,
            'date_created'=>'Wed, 04 Apr 2018 23:51:08 +0000',
            'date_modified'=>'Wed, 04 Apr 2018 23:51:12 +0000',
            'date_shipped'=>'',
            'status_id'=>11,
            'status'=>'Awaiting Fulfillment',
            'subtotal_ex_tax'=>'119.9500',
            'subtotal_inc_tax'=>'119.9500',
            'subtotal_tax'=>'0.0000',
            'base_shipping_cost'=>'9.9500',
            'shipping_cost_ex_tax'=>'9.9500',
            'shipping_cost_inc_tax'=>'9.9500',
            'shipping_cost_tax'=>'0.0000',
            'shipping_cost_tax_class_id'=>2,
            'base_handling_cost'=>'0.0000',
            'handling_cost_ex_tax'=>'0.0000',
            'handling_cost_inc_tax'=>'0.0000',
            'handling_cost_tax'=>'0.0000',
            'handling_cost_tax_class_id'=>2,
            'base_wrapping_cost'=>'0.0000',
            'wrapping_cost_ex_tax'=>'0.0000',
            'wrapping_cost_inc_tax'=>'0.0000',
            'wrapping_cost_tax'=>'0.0000',
            'wrapping_cost_tax_class_id'=>3,
            'total_ex_tax'=>'117.9000',
            'total_inc_tax'=>'117.9000',
            'total_tax'=>'0.0000',
            'items_total'=>1,
            'items_shipped'=>0,
            'payment_method'=>'Test Payment Gateway',
            'payment_provider_id'=>'',
            'payment_status'=>'captured',
            'refunded_amount'=>'0.0000',
            'order_is_digital'=>false,
            'store_credit_amount'=>'0.0000',
            'gift_certificate_amount'=>'0.0000',
            'ip_address'=>'64.183.182.114',
            'geoip_country'=>'United States',
            'geoip_country_iso2'=>'US',
            'currency_id'=>1,
            'currency_code'=>'USD',
            'currency_exchange_rate'=>'1.0000000000',
            'default_currency_id'=>1,
            'default_currency_code'=>'USD',
            'staff_notes'=>'',
            'customer_message'=>'',
            'discount_amount'=>'12.0000',
            'coupon_discount'=>'0.0000',
            'shipping_address_count'=>1,
            'is_deleted'=>false,
            'ebay_order_id'=>'0',
            'cart_id'=>'adaf1875-7e55-49f4-ab55-6f264390b158',
            'billing_address'=>{
                'first_name'=>'Juanita',
                'last_name'=>'Jones',
                'company'=>'',
                'street_1'=>'180 County Road 155',
                'street_2'=>'',
                'city'=>'Georgetown',
                'state'=>'Texas',
                'zip'=>'78626',
                'country'=>'United States',
                'country_iso2'=>'US',
                'phone'=>'',
                'email'=>'customer7DVT05CT@gmail.com',
                'form_fields'=>[

                ]
            },
            'is_email_opt_in'=>false,
            'credit_card_type'=>nil,
            'order_source'=>'www',
            'channel_id'=>1,
            'external_source'=>nil,
            'products'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/116/products.json',
                'resource'=>'/orders/116/products'
            },
            'shipping_addresses'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/116/shippingaddresses.json',
                'resource'=>'/orders/116/shippingaddresses'
            },
            'coupons'=>{
                'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/116/coupons.json',
                'resource'=>'/orders/116/coupons'
            },
            'external_id'=>nil,
            'external_merchant_id'=>nil,
            'tax_provider_id'=>'',
            'custom_status'=>'Awaiting Fulfillment'
        },
    ]    
  end

  let(:order) do
    {
        'id'=>115,
        'customer_id'=>10,
        'date_created'=>'Wed, 04 Apr 2018 23:50:45 +0000',
        'date_modified'=>'Wed, 04 Apr 2018 23:50:50 +0000',
        'date_shipped'=>'',
        'status_id'=>11,
        'status'=>'Awaiting Fulfillment',
        'subtotal_ex_tax'=>'56.5000',
        'subtotal_inc_tax'=>'56.5000',
        'subtotal_tax'=>'0.0000',
        'base_shipping_cost'=>'9.9500',
        'shipping_cost_ex_tax'=>'9.9500',
        'shipping_cost_inc_tax'=>'9.9500',
        'shipping_cost_tax'=>'0.0000',
        'shipping_cost_tax_class_id'=>2,
        'base_handling_cost'=>'0.0000',
        'handling_cost_ex_tax'=>'0.0000',
        'handling_cost_inc_tax'=>'0.0000',
        'handling_cost_tax'=>'0.0000',
        'handling_cost_tax_class_id'=>2,
        'base_wrapping_cost'=>'0.0000',
        'wrapping_cost_ex_tax'=>'0.0000',
        'wrapping_cost_inc_tax'=>'0.0000',
        'wrapping_cost_tax'=>'0.0000',
        'wrapping_cost_tax_class_id'=>3,
        'total_ex_tax'=>'66.4500',
        'total_inc_tax'=>'66.4500',
        'total_tax'=>'0.0000',
        'items_total'=>2,
        'items_shipped'=>0,
        'payment_method'=>'Test Payment Gateway',
        'payment_provider_id'=>'',
        'payment_status'=>'captured',
        'refunded_amount'=>'0.0000',
        'order_is_digital'=>false,
        'store_credit_amount'=>'0.0000',
        'gift_certificate_amount'=>'0.0000',
        'ip_address'=>'64.183.182.114',
        'geoip_country'=>'United States',
        'geoip_country_iso2'=>'US',
        'currency_id'=>1,
        'currency_code'=>'USD',
        'currency_exchange_rate'=>'1.0000000000',
        'default_currency_id'=>1,
        'default_currency_code'=>'USD',
        'staff_notes'=>'',
        'customer_message'=>'',
        'discount_amount'=>'0.0000',
        'coupon_discount'=>'0.0000',
        'shipping_address_count'=>1,
        'is_deleted'=>false,
        'ebay_order_id'=>'0',
        'cart_id'=>'64c726f6-490e-47ac-86b9-a9eefb6f8c8a',
        'billing_address'=>{
            'first_name'=>'Juanita',
            'last_name'=>'Jones',
            'company'=>'',
            'street_1'=>'180 County Road 155',
            'street_2'=>'',
            'city'=>'Georgetown',
            'state'=>'Texas',
            'zip'=>'78626',
            'country'=>'United States',
            'country_iso2'=>'US',
            'phone'=>'',
            'email'=>'customer7DVT05CT@gmail.com',
            'form_fields'=>[

            ]
        },
        'is_email_opt_in'=>false,
        'credit_card_type'=>nil,
        'order_source'=>'www',
        'channel_id'=>1,
        'external_source'=>nil,
        'products'=>{
            'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/115/products.json',
            'resource'=>'/orders/115/products'
        },
        'shipping_addresses'=>{
            'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/115/shippingaddresses.json',
            'resource'=>'/orders/115/shippingaddresses'
        },
        'coupons'=>{
            'url'=>'https://dominica-ernsers-store.mybigcommerce.com/api/v2/orders/115/coupons.json',
            'resource'=>'/orders/115/coupons'
        },
        'external_id'=>nil,
        'external_merchant_id'=>nil,
        'tax_provider_id'=>'',
        'custom_status'=>'Awaiting Fulfillment'
    }
  end

  describe '#order_list' do

    it 'parses data and returns order collection' do
      parser = subject.new(data: orders)
      order_list = parser.order_list

      expect(order_list).to be_an_instance_of(Array)
      expect(order_list.size).to eq 2
      expect(order_list.first).to be_an_instance_of(Order)
      expect(order_list.last).to be_an_instance_of(Order)
      expect(order_list.first.id).to eq 115
      expect(order_list.last.id).to eq 116
    end
  end
end
