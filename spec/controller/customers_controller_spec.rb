# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CustomersController, type: :controller do

  describe '#index' do

    it 'should respond successfully' do
      get :index
      expect(response.status).to eq 200
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end

    context 'when fetching customers list' do
      let(:customer) { Customer.new }

      before do
        allow_any_instance_of(CustomerService).to receive(:customers_list) { [customer] }
      end

      it 'assigns @customers' do

        get :index
        expect(assigns(:customers)).to eq([customer])
      end
    end
  end

  describe '#show' do
    let(:customer) { Customer.new }

    before do
      customer.id = 1
      allow_any_instance_of(CustomerService).to receive(:customer) { customer }
    end

    it 'should respond successfully' do

      get :show, params: { id: customer.id}
      expect(response.status).to eq 200
    end

    it 'renders the show template' do

      get :show, params: { id: customer.id }
      expect(response).to render_template('show')
    end

    it 'assigns @customer' do
      get :show, params: { id: customer.id }
      expect(assigns(:customer)).to eq(customer)
    end
  end

end