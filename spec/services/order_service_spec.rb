# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderService do

  subject { described_class }

  describe '#orders_list' do
    let(:service) { subject.new(params: {customer_id: '1'}) }

    it 'fetches order data list from api client' do
      expect(service.api_client).to receive(:orders) { [].to_s }
      service.orders_list
    end

    it 'creates order parser and calls its order_list method' do
      data = JSON.parse([].to_s)
      parser = OrderParser.new(data: data)
      allow(service.api_client).to receive(:orders) { [].to_s }
      expect(OrderParser).to receive(:new).with(data: data) { parser }
      expect(parser).to receive(:order_list)
      service.orders_list
    end
  end

  describe '#orders_count' do
    let(:service) { subject.new(params: {customer_id: '1'}) }
    let(:orders) { [double('order')].to_json }

    it 'returns count of customer orders' do
      allow(service.api_client).to receive(:orders) { orders }
      expect(service.orders_count).to eq 1
    end
  end

  describe '#order_products_count' do
    let(:service) { subject.new(params: {customer_id: '1', order_id: '1'}) }

    it 'calls order_products_count of api client' do
      expect(service.api_client).to receive(:order_products_count)
      service.order_products_count
    end
  end
end
