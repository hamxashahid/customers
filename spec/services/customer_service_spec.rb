# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CustomerService do

  subject { described_class }

  describe '#customers_list' do
    let(:service) { subject.new(params: {customer_id: '1'}) }

    it 'fetches customer data list from api client' do
      expect(service.api_client).to receive(:customers) { [].to_s }
      service.customers_list
    end

    it 'creates customer parser and calls its customer_list method' do
      data = JSON.parse([].to_s)
      parser = CustomerParser.new(data: data)
      allow(service.api_client).to receive(:customers) { [].to_s }
      expect(CustomerParser).to receive(:new).with(data: data) { parser }
      expect(parser).to receive(:customer_list)
      service.customers_list
    end
  end

  describe '#customer' do
    let(:service) { subject.new(params: {customer_id: '1'}) }

    it 'fetches customer data from api client' do
      expect(service.api_client).to receive(:customer) { {}.to_s }
      service.customer
    end

    it 'creates customer parser and calls its customer method' do
      data = JSON.parse({}.to_s)
      parser = CustomerParser.new(data: data)
      allow(service.api_client).to receive(:customer) { {}.to_s }
      expect(CustomerParser).to receive(:new).with(data: data) { parser }
      expect(parser).to receive(:customer)
      service.customer
    end
  end
end
