# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Customer do

  subject { described_class.new }

  before do
    subject.id = 1
  end

  describe '#full_name' do

    it 'returns first name and last name' do
      subject.first_name = 'Jon'
      subject.last_name = 'Snow'

      expect(subject.full_name).to eq('Jon Snow')
    end
  end

  describe '#orders' do
    let(:order_service) { OrderService.new(params: {customer_id: subject.id}) }

    before do
      allow(subject).to receive(:order_service) { order_service }
    end

    it 'calls orders_list method of order service' do
      expect(order_service).to receive(:orders_list)
      subject.orders
    end

  end

  describe '#orders_count' do
    let(:order_service) { OrderService.new(params: {customer_id: subject.id}) }

    before do
      allow(subject).to receive(:order_service) { order_service }
    end

    it 'calls orders_count method of order service' do
      expect(order_service).to receive(:orders_count)
      subject.orders_count
    end
  end
end