# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Order do

  subject { described_class.new }

  before do
    subject.id = 1
    subject.customer_id = 1
  end

  describe '#products_count' do
    let(:order_service) { OrderService.new(params: {order_id: subject.id, customer_id: subject.customer_id}) }

    before do
      allow(subject).to receive(:order_service) { order_service }
    end

    it 'calls orders_count method of order service' do
      expect(order_service).to receive(:order_products_count)
      subject.product_count
    end
  end
end
